const { app, BrowserWindow } = require("electron");
const { Menu, Tray, globalShortcut } = require("electron");
const path = require("path");
require("core-js/stable");
require("regenerator-runtime/runtime");
const isDev = require("electron-is-dev");
const { autoUpdater } = require("electron-updater");
const log = require("electron-log");
const { MenuBuilder } = require("./menu");

class AppUpdater {
  constructor() {
    log.transports.file.level = "info";
    autoUpdater.logger = log;
    autoUpdater.checkForUpdatesAndNotify();
  }
}

// Create the browser window.
let mainWindow = null;

if (process.env.NODE_ENV === "production") {
  const sourceMapSupport = require("source-map-support");
  sourceMapSupport.install();
}

if (
  process.env.NODE_ENV === "development" ||
  process.env.DEBUG_PROD === "true"
) {
  require("electron-debug")();
}

const installExtensions = async () => {
  const installer = require("electron-devtools-installer");
  const forceDownload = !!process.env.UPGRADE_EXTENSIONS;
  const extensions = ["REACT_DEVELOPER_TOOLS", "REDUX_DEVTOOLS"];

  return Promise.all(
    extensions.map((name) => installer.default(installer[name], forceDownload))
  ).catch(console.log);
};

const createWindow = async () => {
  if (
    process.env.NODE_ENV === "development" ||
    process.env.DEBUG_PROD === "true"
  ) {
    await installExtensions();
  }

  mainWindow = new BrowserWindow({
    show: false,
    width: 1024,
    height: 728,
    webPreferences: {
      nodeIntegration: true,
      // preload: path.join(__dirname, 'preload.js'),
    },
    // (process.env.NODE_ENV === 'development' ||
    //   process.env.E2E_BUILD === 'true') &&
    // process.env.ERB_SECURE !== 'true'
    //   ? {
    //       nodeIntegration: true,
    //     }
    //   : {
    //       preload: path.join(__dirname, 'dist/renderer.prod.js'),
    //     },
  });

  // and load the index.html of the app.
  mainWindow.loadURL(
    isDev
      ? "http://localhost:3000"
      : `file://${path.join(__dirname, "../build/index.html")}`
  );
  if (isDev) mainWindow.webContents.openDevTools();
  mainWindow.webContents.on("did-finish-load", () => {
    if (!mainWindow) {
      throw new Error('"mainWindow" is not defined');
    }
    if (process.env.START_MINIMIZED) {
      mainWindow.minimize();
    } else {
      mainWindow.show();
      mainWindow.focus();
    }
  });

  mainWindow.on("closed", () => {
    mainWindow = null;
  });
  const menuBuilder = new MenuBuilder(mainWindow);
  menuBuilder.buildMenu();
  new AppUpdater();
  // Open the DevTools.
};
app.on("window-all-closed", () => {
  // Respect the OSX convention of having the application in memory even
  // after all windows have been closed
  if (process.platform !== "darwin") {
    app.quit();
  }
});
if (process.env.E2E_BUILD === "true") {
  app.whenReady().then(createWindow);
} else {
  app.on("ready", () => {
    createWindow();
    // const tray = new Tray(path.join("public", "logo192.png"));
    // const contextMenu = Menu.buildFromTemplate([
    //   { label: "Item1", type: "radio" },
    //   { label: "Item2", type: "radio" },
    //   { label: "Item3", type: "radio", checked: true },
    //   { label: "Item4", type: "radio" },
    // ]);
    // tray.setToolTip("This is my application.");

    // tray.setContextMenu(contextMenu);

    // const ret = globalShortcut.register("a", () => {
    //   console.log("CommandOrControl+X is pressed");
    // });

    // if (!ret) {
    //   console.log("registration failed");
    // }
    // console.log(globalShortcut.isRegistered("CommandOrControl+X"));
  });
}
// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
// app.whenReady().then(createWindow)

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on("window-all-closed", () => {
  if (process.platform !== "darwin") {
    app.quit();
  }
});

app.on("activate", () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow();
  }
});
