import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { compose } from "recompose";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { clipboard, remote, nativeImage } from "electron";

import {
  exampleDataSelector,
  fetchingSelector,
} from "../selectors/exampleSelector";

import { item } from "../actions";
import LoadingAnimation from "../components/LoadingAnimation";
import WithErrors from "../hocs/WithErrors";
import mergeImg from "merge-img";
const screenshot = require("screenshot-desktop");

const { Tray, Menu } = remote;
const ExampleContainer = (props) => {
  const [image, setImage] = useState(false);

  useEffect(() => {
    clipboard.writeText("Example String", "selection");
    console.log(clipboard.readText("selection"));
    let trayIcon = new Tray(
      "/Users/vuhoi/Desktop/project/electron-react/public/logo192.png"
    );

    const trayMenuTemplate = [
      {
        label: "Empty Application",
        enabled: false,
      },

      {
        label: "Settings",
        click: function () {
          console.log("Clicked on settings");
        },
      },

      {
        label: "Help",
        click: function () {
          console.log("Clicked on Help");
        },
      },
    ];

    let trayMenu = Menu.buildFromTemplate(trayMenuTemplate);
    trayIcon.setContextMenu(trayMenu);

    // const BrowserWindow = remote.BrowserWindow;
    // const win = BrowserWindow.getFocusedWindow();

    // let win = BrowserWindow.getAllWindows()[0];

    // win.webContents.on("before-input-event", (event, input) => {
    //   console.log(input);
    // });

    let i = 0;
    setInterval(() => {
      i++;
      screenshot({
        format: "png",
        // filename: `public/assets/screenshot/screen${i}.png`,
      })
        .then((img) => {
          // console.log(img);
          // trayIcon.setImage(nativeImage.createFromBuffer(img));
          const bytes = new Uint8Array(img);
          setImage(
            "data:image/png;base64," + String.fromCharCode.apply(null, bytes)
          );
          console.log("ss");
        })
        .catch((err) => {
          // ...
          console.log(err);
        });
    }, 5000);

    // mergeImg(["public/logo192.png", "public/logo512.png"]).then((img) => {
    //   // Save image as file
    //   console.log(img);
    //   img.write("public/out.png", () => console.log("done"));
    // });
  }, []);

  const { exampleData, fetching } = props;

  if (fetching) return <LoadingAnimation />;

  return (
    <div>
      <h1>Example Container</h1>
      <br />
      <div className="row">
        <div className="card">
          <h4 className="card-header">Example Data:</h4>
          <div className="card-body">
            <h4 className="card-title">{exampleData.title}</h4>
            <p className="card-text">{exampleData.body}</p>
            <Link to="/exampleComponent" className="btn btn-warning">
              Link to Example Component
            </Link>
            {image.length > 0 && (
              <img
                style={{ width: "100%", objectFit: "cover" }}
                src={image.split("public")[1]}
              />
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = createStructuredSelector({
  exampleData: exampleDataSelector(),
  fetching: fetchingSelector(),
});

const mapDispatchToProps = {
  loadOne: item.requestOne,
};

export default compose(
  WithErrors,
  connect(mapStateToProps, mapDispatchToProps)
)(ExampleContainer);
