import React, { useEffect } from "react";
import "./App.css";

import Home from "./containers/home";
import { clipboard } from "electron";
const screenshot = require("screenshot-desktop");
const Mousetrap = require("mousetrap");

export const AppContext = React.createContext();
class AppProvider extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <AppContext.Provider value={this.state}>
        {this.props.children}
      </AppContext.Provider>
    );
  }
}

function App() {
  useEffect(() => {
    clipboard.writeText("Example String", "selection");
    console.log(clipboard.readText("selection"));

    // const remote=require('electron').remote;
    //     const BrowserWindow=remote.BrowserWindow;
    //     const win = new BrowserWindow({
    //       height: 200,
    //       width: 800,
    //       frame:false,
    //       webPreferences: {
    //         nodeIntegration: true,
    //     }
    //     });
    //     win.loadURL('https://stackoverflow.com');
    let i = 0;
    setInterval(() => {
      i++;
      screenshot({ format: "png", filename: `screenshot/screen${i}.png` })
        .then((img) => {
          console.log(img);
        })
        .catch((err) => {
          // ...
        });
    }, 10000);

    Mousetrap.bind(
      "esc",
      function () {
        console.log("escape");
      },
      "keyup"
    );
    // const BrowserWindow = remote.BrowserWindow;
    // const win = BrowserWindow.getFocusedWindow();

    // // let win = BrowserWindow.getAllWindows()[0];

    // win.webContents.on("before-input-event", (event, input) => {
    //     console.log(input);
    // });

    // window.addEventListener('keyup', ()=>console.log('hello'), true)
  }, []);
  return (
    <AppProvider>
      <AppContext.Consumer>
        <Switch>
          <Route exact path="/" component={Home} />
        </Switch>
      </AppContext.Consumer>
    </AppProvider>
  );
}

export default App;
